package com.example.unlockapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.os.Build;
import android.os.Bundle;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.common.StringUtils;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity { //AppCompatActivity {


    //Info
    //Zufallszahl -> Pattern-Listennummer patternArray
    //MainActivityDanach -> geänderter Farbcode ChangeColorCodeActivity
    //HexKey -> aktueller ColorCode
    //Mein Satz -> sentColors


    PatternLockView patternLockView;
    ImageView qrImage;
    //public static String[] myColors = {"#FFFF00", "#FF0000", "#4CFF00","#00BCD4", "#FF9800", "#9C27B0", "#01CCB9", "#0323CA", "#FF00DD"};
    public static int[] hexKey2 = {R.color.yellow, R.color.red, R.color.green, R.color.blue};
    public static String[] hexKey = {"#FFFF00", "#FF0000", "#4CFF00","#00BCD4"};
    int patternLength = 4; //hexKey.length?
    String[] patternArray = {"0123", "0367", "2467", "1476", "8765"}; //extending patterns -> change randomInt range in generateUnlockCode()
    int currentPatternNr;
    public ArrayList<Integer> myColors;
    String[] sentColors;
    int[] sentColors1;
    String[] colorCode = new String[9];


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().hide();

        qrImage = findViewById(R.id.iv_output);
        patternLockView = (PatternLockView) findViewById(R.id.pattern_lock_view);

        patternLockView.addPatternLockListener(mPatternLockViewListener);

        generateUnlockCode();
    }


    @Override
    protected void onResume() {
        super.onResume();

        //Random Pattern
        generateUnlockCode();

        // QR-Code
        //".join" requires java 8
        //String hexKeyString = String.join("", hexKey);
        //Log.i("HexKey", hexKeyString);

        //String colorCode = String.join("", sentColors);

        //String colorCodeString = String.join("", colorCodeNew);
        //Log.i("Mein Satz", colorCodeString);
/*
        String[] colorCodeNew = createHexColorCode();
        String colorCodeString="";
        for(int i=0; i<colorCodeNew.length; i++){
            colorCodeString=colorCodeString+colorCodeNew[i];
        }
        Log.i("COLORCODESTRING", colorCodeString);*/
        createHexColorCode();
        String colorCodeString="";
        for(int i=0; i<colorCode.length; i++){
            colorCodeString=colorCodeString+colorCode[i];
        }
        Log.i("COLORCODESTRING", colorCodeString);

        QRGEncoder qrgEncoder = new QRGEncoder (colorCodeString, null, QRGContents.Type.TEXT, 300);
        // Getting QR-Code as Bitmap
        Bitmap bitmap = qrgEncoder.getBitmap();
        // Setting Bitmap to ImageView
        qrImage.setImageBitmap(bitmap);
    }

    private void createHexColorCode() {

        for(int i=0; i<sentColors1.length; i++){
            if(sentColors1[i]== R.color.yellow){
                colorCode[i]= "#FFFF00";
            }
            else if(sentColors1[i]== R.color.red){
                colorCode[i]= "#FF0000";
            }
            else if(sentColors1[i]== R.color.green){
                colorCode[i]= "#4CFF00";
            }
            else if(sentColors1[i]== R.color.blue){
                colorCode[i]= "#00BCD4";
            }
            else if(sentColors1[i]== R.color.orange){
                colorCode[i]= "#FF9800";
            }
            else if(sentColors1[i]== R.color.purple){
                colorCode[i]= "#9C27B0";
            }
            else if(sentColors1[i]== R.color.turquoise){
                colorCode[i]= "#01CCB9";
            }
            else if(sentColors1[i]== R.color.darkblue){
                colorCode[i]= "#0323CA";
            }
            else if(sentColors1[i]== R.color.pink){
                colorCode[i]= "#FF00DD";
            }

        }
    }


    private void openChangeColorCodeActivity() {
        Intent intent = new Intent(this, ChangeColorCodeActivity.class);
        startActivity(intent);
    }

    private void generateUnlockCode() {
        // define the range
        int max = 4;
        int min = 0;
        int range = max - min + 1;

        int randomInt = (int) (Math.random() * range) + min;
        Log.i("Zufallszahl", String.valueOf(randomInt));
        currentPatternNr = randomInt;                                     //e.g. randomInt= 3 -> pattern: "1476"

        String[] split = patternArray[randomInt].split("");         //e.g. split= ["1","4","7","6"]
        //String[] colors = new String[9];
        int[] colors = new int[9];

        for(int i=0; i<patternLength; i++){
            //colors[Integer.parseInt(split[i])] = hexKey[i];              //e.g. colors = [null,"FFFF00",null,null,"#FF0000",null,"#00BCD4","#4CFF00",null]
            colors[Integer.parseInt(split[i])] = hexKey2[i];               //e.g. colors = [0, R.color.yellow, 0, 0, R.color.red, 0, R.color.blue, R.color.green, 0]
        }

        myColors = new ArrayList<Integer>() {{
            add(R.color.yellow);
            add(R.color.red);
            add(R.color.green);
            add(R.color.blue);
            add(R.color.orange);
            add(R.color.purple);
            add(R.color.turquoise);
            add(R.color.darkblue);
            add(R.color.pink);
        }};
        /*
        ArrayList<String> a = new ArrayList<String>();
        {
            myColors = new ArrayList() {{
                add("#FFFF00");
                add("#FF0000");
                add("#4CFF00");
                add("#00BCD4");
                add("#FF9800");
                add("#9C27B0");
                add("#01CCB9");
                add("#0323CA");
                add("#FF00DD");
            }};
        }

        //find colors that are not taken by ColorCode -> for remaining places in array
        /*
        for(int j=0; j<hexKey.length; j++) {
            for(int i=0; i<myColors.size(); i++){
                if(hexKey[j]==myColors.get(i)){
                    myColors.remove(hexKey[j]);                         //e.g. myColors= ["#FF9800", "#9C27B0", "#01CCB9", "#0323CA", "#FF00DD"]
                }
            }
        }*/
        for(int j=0; j<hexKey2.length; j++) {
            for(int i=0; i<myColors.size(); i++){
                if(hexKey2[j]==myColors.get(i)){
                    myColors.remove(i);                         //e.g. myColors= ["#FF9800", "#9C27B0", "#01CCB9", "#0323CA", "#FF00DD"]
                }
            }
        }


        //define colors for places in color-Array (sent via QR-Code) that are currently null -> random selection out of defined rest color ArrayList
        for(int x=0; x<colors.length; x++){
            if(colors[x]== 0){
                // define the range
                int maxi = myColors.size()-1;
                int mini = 0;
                int myrange = maxi - mini + 1;
                int myrandomInt = (int) (Math.random() * myrange) + mini;

                int pickedColor = myColors.get(myrandomInt);
                //String pickedColor = (String) myColors.get(myrandomInt);
                colors[x] = pickedColor;
                myColors.remove(myrandomInt);
                //myColors.remove(""+ pickedColor);
            }
        }

        //sentColors = colors;
        sentColors1 = colors;
    }



    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {

        @Override
        public void onStarted() {

        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {

        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            if (PatternLockUtils.patternToString(patternLockView, pattern).equalsIgnoreCase(patternArray[currentPatternNr])) {
                patternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
                Toast.makeText(MainActivity.this, "Correct", Toast.LENGTH_SHORT).show();
                openChangeColorCodeActivity();
            } else {
                patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                Toast.makeText(MainActivity.this, "Incorrect", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCleared() {

        }
    };
}