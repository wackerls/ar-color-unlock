package com.example.unlockapp;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class ChangeColorCodeActivity extends Activity { //AppCompatActivity  {


    public Button button1;
    public Button button2;
    public Button button3;
    public Button button4;
    public Button buttonChangeColors;
    private String[] localhexKey = {"#FFFF00", "#FF0000", "#4CFF00","#00BCD4"};
    public int[] localhexKey2 = {R.color.yellow, R.color.red, R.color.green, R.color.blue};

    private String[] temp = {"#FFFF00", "#FF0000", "#4CFF00","#00BCD4"};
    public int[] tempColors = {R.color.yellow, R.color.red, R.color.green, R.color.blue};
    public static String[] myColorsHex = {"#FFFF00", "#FF0000", "#4CFF00","#00BCD4", "#FF9800", "#9C27B0", "#01CCB9", "#0323CA", "#FF00DD"};
    public ArrayList<Integer> myColors;
    //private String[] localhexKey = MainActivity.hexKey;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_color_code);
        getActionBar().hide();

        button1 = findViewById(R.id.button_id1);
        button2 = findViewById(R.id.button_id2);
        button3 = findViewById(R.id.button_id3);
        button4 = findViewById(R.id.button_id4);
        buttonChangeColors = findViewById(R.id.button_changeColorCode);


        onClickBtn(button1);
        onClickBtn(button2);
        onClickBtn(button3);
        onClickBtn(button4);
        saveNewColorCode(buttonChangeColors);

        String colorCode = String.join("", MainActivity.hexKey);
        Log.i("ChangeColorDavor", colorCode);

        button1.setBackgroundTintList(getResources().getColorStateList(MainActivity.hexKey2[0]));
        button2.setBackgroundTintList(getResources().getColorStateList(MainActivity.hexKey2[1]));
        button3.setBackgroundTintList(getResources().getColorStateList(MainActivity.hexKey2[2]));
        button4.setBackgroundTintList(getResources().getColorStateList(MainActivity.hexKey2[3]));


    }



    private void onClickBtn(Button button){

        button.setOnClickListener(new View.OnClickListener() {
            int i = 0;

            public void onClick(View v) {
                myColors = new ArrayList<Integer>() {{
                    add(R.color.yellow);
                    add(R.color.red);
                    add(R.color.green);
                    add(R.color.blue);
                    add(R.color.orange);
                    add(R.color.purple);
                    add(R.color.turquoise);
                    add(R.color.darkblue);
                    add(R.color.pink);
                }};

                for(int x=0; x<myColors.size()-1; x++){
                    for(int y=0; y<tempColors.length; y++){
                        if(tempColors[y]==myColors.get(x)){
                            myColors.remove(x);                         //e.g. myColors= ["#FF9800", "#9C27B0", "#01CCB9", "#0323CA", "#FF00DD"]
                        }
                    }
                }

                if(i%5 == 0){
                    button.setBackgroundTintList(getResources().getColorStateList(myColors.get(0)));
                    changeTempColorCode(button, myColors.get(0));
                }
                if(i%5 == 1) {
                    button.setBackgroundTintList(getResources().getColorStateList(myColors.get(1)));
                    changeTempColorCode(button, myColors.get(1));
                }
                if(i%5 == 2) {
                    button.setBackgroundTintList(getResources().getColorStateList(myColors.get(2)));
                    changeTempColorCode(button, myColors.get(2));
                }
                if(i%5 == 3) {
                    button.setBackgroundTintList(getResources().getColorStateList(myColors.get(3)));
                    changeTempColorCode(button, myColors.get(3));
                }
                if(i%5 == 4) {
                    button.setBackgroundTintList(getResources().getColorStateList(myColors.get(4)));
                    changeTempColorCode(button, myColors.get(4));
                }

                i++;
            }

            private void changeTempColorCode(Button button, int color) {
                if (button1.equals(button)) {
                    tempColors[0] = color;
                } else if (button2.equals(button)) {
                    tempColors[1] = color;
                } else if (button3.equals(button)){
                    tempColors[2] = color;
                } else if (button4.equals(button)) {
                    tempColors[3] = color;
                }
            }
        });
    }

    private void saveNewColorCode(Button buttonChangeColors) {
        buttonChangeColors.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                MainActivity.hexKey2 = tempColors;
                Toast.makeText(ChangeColorCodeActivity.this, "Color Code Saved", Toast.LENGTH_SHORT).show();
                //MainActivity.hexKey = localhexKey;

/*
                for(int i=0; i<tempColors.length; i++){
                    if(tempColors[i]== R.color.yellow){
                        localhexKey[i]= "#FFFF00";
                    }
                    else if(tempColors[i]== R.color.red){
                        localhexKey[i]= "#FF0000";
                    }
                    else if(tempColors[i]== R.color.green){
                        localhexKey[i]= "#4CFF00";
                    }
                    else if(tempColors[i]== R.color.blue){
                        localhexKey[i]= "#00BCD4";
                    }
                    else if(tempColors[i]== R.color.orange){
                        localhexKey[i]= "#FF9800";
                    }
                    else if(tempColors[i]== R.color.purple){
                        localhexKey[i]= "#9C27B0";
                    }
                    else if(tempColors[i]== R.color.turquoise){
                        localhexKey[i]= "#01CCB9";
                    }
                    else if(tempColors[i]== R.color.darkblue){
                        localhexKey[i]= "#0323CA";
                    }
                    else if(tempColors[i]== R.color.pink){
                        localhexKey[i]= "#FF00DD";
                    }

                }
                MainActivity.hexKey = localhexKey;
                if(tempColors[0] == R.color.yellow){

                }*/


            }
        });
    }
}