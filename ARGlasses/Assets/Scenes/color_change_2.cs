using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//using SimpleDemo;

public class color_change_2 : MonoBehaviour
{
    float[] getcolors(string hexcode)
    {
        string r = "";
        string g = "";
        string b = "";

        r += hexcode[0] + hexcode[1];
        g += hexcode[2] + hexcode[3];
        b += hexcode[4] + hexcode[5];

        int r_int = System.Convert.ToInt32(r, 16);
        float r_f = r_int / 256;
        int g_int = System.Convert.ToInt32(g, 16);
        float g_f = g_int / 256;
        int b_int = System.Convert.ToInt32(b, 16);
        float b_f = b_int / 256;

        float[] farben = new float[3];
        farben[0] = r_f;
        farben[1] = g_f;
        farben[2] = b_f;
        return farben;
    }
    //GameObject QRCODE = GameObject.Find("");
    //public static void QR_text = SimpleDemo.QRInhalt;
    void Start()
    {
        //GameObject pattern = GameObject.Find("pattern");
        //var patternrenderer = pattern.GetComponent<Renderer>();
        //patternrenderer.material.SetColor("_Color", Color.red);

        GameObject mitte = GameObject.Find("Cylinder");

        GameObject rechts = GameObject.Find("Cylinder_r");
        GameObject links = GameObject.Find("Cylinder_l");
        GameObject oben = GameObject.Find("Cylinder_u");
        GameObject unten = GameObject.Find("Cylinder_o");
        GameObject or = GameObject.Find("Cylinder_ur");
        GameObject ol = GameObject.Find("Cylinder_ul");
        GameObject ur = GameObject.Find("Cylinder_or");
        GameObject ul = GameObject.Find("Cylinder_ol");

        //Get the Renderer component from the new cube
        var mitteRenderer = mitte.GetComponent<Renderer>();
        var rechtsRenderer = rechts.GetComponent<Renderer>();
        var linksRenderer = links.GetComponent<Renderer>();
        var untenRenderer = unten.GetComponent<Renderer>();
        var obenRenderer = oben.GetComponent<Renderer>();
        var urRenderer = ur.GetComponent<Renderer>();
        var ulRenderer = ul.GetComponent<Renderer>();
        var orRenderer = or.GetComponent<Renderer>();
        var olRenderer = ol.GetComponent<Renderer>();


        string[] farben = new string[9];
        string farbenlang = container.CrossSceneInformation;
        //string farbenlang = "#FFFF00#FF0000#4CFF00#00BCD4#9C27B0#01CCB9#FF00DD#0323CA#FF9800";
        //string farbenlang = "#FF9800" + "#FFFF00" + "#4CFF00" + "#00BCD4"+"#9C27B0"+"#01CCB9"+"#0323CA"+"#FF00DD"+"#FF0000";
        //farben = new string[] { "000000", "222222", "444444", "666666", "888888", "AAAAAA", "CCCCCC", "DDDDDD", "FFFFFF" };
        farben[0] = farbenlang.Substring(1, 6);
        farben[1] = farbenlang.Substring(8, 6);
        farben[2] = farbenlang.Substring(15, 6);
        farben[3] = farbenlang.Substring(22, 6);
        farben[4] = farbenlang.Substring(29, 6);
        farben[5] = farbenlang.Substring(36, 6);
        farben[6] = farbenlang.Substring(43, 6);
        farben[7] = farbenlang.Substring(50, 6);
        farben[8] = farbenlang.Substring(57, 6);
        Console.WriteLine(farben);
        Debug.Log(farben);

        Color[] farben_color = new Color[9];

        for (int i = 0; i < 9; i++)
        {
            if (farben[i] == "FFFF00") { farben_color[i] = new Color(1f, 1f, 0f, 0.5f); }
            if (farben[i] == "FF0000") { farben_color[i] = new Color(1f, 0f, 0f, 0.5f); }
            if (farben[i] == "4CFF00") { farben_color[i] = new Color(0.3f, 1f, 0f, 0.5f); }
            if (farben[i] == "00BCD4") { farben_color[i] = new Color(0f, 0.74f, 0.83f, 0.5f); }
            if (farben[i] == "FF9800") { farben_color[i] = new Color(1f, 0.6f, 0f, 0.5f); }
            if (farben[i] == "9C27B0") { farben_color[i] = new Color(0.6f, 0.15f, 0.7f, 0.5f); }
            if (farben[i] == "01CCB9") { farben_color[i] = new Color(0f, 0.8f, 0.73f, 0.5f); }
            if (farben[i] == "0323CA") { farben_color[i] = new Color(0.01f, 0.17f, 0.8f, 0.5f); }
            if (farben[i] == "FF00DD") { farben_color[i] = new Color(1f, 0f, 0.87f, 0.5f); }
        }


        olRenderer.material.SetColor("_Color", farben_color[0]);
        obenRenderer.material.SetColor("_Color", farben_color[1]);
        orRenderer.material.SetColor("_Color", farben_color[2]);
        linksRenderer.material.SetColor("_Color", farben_color[3]);
        mitteRenderer.material.SetColor("_Color", farben_color[4]);
        rechtsRenderer.material.SetColor("_Color", farben_color[5]);
        ulRenderer.material.SetColor("_Color", farben_color[6]);
        untenRenderer.material.SetColor("_Color", farben_color[7]);
        urRenderer.material.SetColor("_Color", farben_color[8]);
        
        
        
    }


}